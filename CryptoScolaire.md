---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: Cryptographie à l'école
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs: 
  - Jacques Supcik
mots-clés: [Didactique, HEP, cryptographie]
langue: [F,E,D]
confidentialité: non
suite: non
---

## Description/Contexte
La Haute École pédagogique de Fribourg (HEP|PH FR) forme des enseignants pour les classes primaires et secondaires. Les élèves de classes de l’école obligatoire auront prochainement une nouvelle discipline à apprendre: les sciences informatiques. L’objectif du projet est de développer, conjointement avec des étudiants de la HEP|PH FR, des environnements numériques et, si possible aussi "analogiques", qui permettent à des élèves du primaire d’acquérir les compétences ciblées. Ainsi, les défis sont certes informatiques, mais aussi didactiques.

La compétence ciblée en 2020/2021 : Crypter et décrypter un message à l’aide de méthodes simples (p. ex.: César, Vigenère, Diffie-Hellman, RSA…)

## Contraintes

- Collaborer avec des étudiants de la HEP|PH FR pour garantir la qualité didactique des activités proposées, ainsi que le développement de matériels
- Prévoir un environnement numérique qui permettra, dans les années à venir, d’accueillir d’autres activités (ciblant d’autres compétences) ;
- Approche de projet suivant une approche "Open Sourse"
- Mettre en production le développement sur les solutions d’hébergement de la HEP|PH FR, afin de pouvoir utiliser la solution développée dans les classes primaires/secondaires du canton de Fribourg.

## Objectifs/Tâches

- Evaluer des méthodes de cryptage variées
- Concevoir des situations didactiques permettant l’apprentissage du fonctionnement des méthodes de cryptage et l’utilisation de celle-ci (crypter un message et le donner à un camarade, décrypter un message)
- Développer les environnements informatisés nécessaires au développement des compétences ciblées en mettant un accent particulier sur la qualité de l'interface utilisateur (IHM)
- Concevoir une architecture logicielle adaptée à la situation dicatique particulière