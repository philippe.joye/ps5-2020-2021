---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: Chargeur de tubes
filières:
  - Informatique
proposé par étudiant: Lucas Bueche
mandants: 
  - Polytype SA
nombre d'étudiants: 1
mots-clés: [Automatique, commande, MCA, Polytyp]
langue: [F,E,D]
confidentialité: non
réalisation: entreprise
suite: non
---


## Description/Contexte
Dans le cadre de la "Motion Control Academy", l'entreprise Polytyp prose de développer avec une équipe interdisciplinaire et sur plusieurs projets d'étudiants un chargeur de tubes automatisé. Il s'agit de concevoir, réaliser et mettre en service un chargeur de tubes. Celui-ci devra prendre les tubes, amenés par une table (tapis roulant), et les charger sur les picots d’une chaîne en mouvement.

## Objectifs/Tâches
L'objectif du projet de semestre est, en collaboration avec les étudiants des filières de Génie Mécanique et de Génie Electrique de :
- Définir le cahier des charges du chargeur de tubes
- Spécifier les fonctions logicielles de l'installation
Plus spécifiquement, traiter de la partie du développement logiciel:
- Proposer une architecture logicielle compatible avec les développements mécaniques et électroniques
- Prise en main OOP pour automates programmables (Simotion)
- Programmation fonctions (selon règles OOP) et HMI
- Définition des fonctions complémentaires (compatible industrie 4.0)
